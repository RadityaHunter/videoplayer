package com.adit.videoplayer.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adit.videoplayer.Helper.GlideApp;
import com.adit.videoplayer.Model.Channel;
import com.adit.videoplayer.PlayVideo.PlayVideoActivity;
import com.adit.videoplayer.R;
import com.adit.videoplayer.databinding.ItemVideosBinding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ViewHolder> {

    private final ArrayList<Channel> listChannel = new ArrayList<>();

    public void setListChannel(ArrayList<Channel> listChannel) {
        this.listChannel.clear();
        this.listChannel.addAll(listChannel);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemVideosBinding binding = ItemVideosBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(listChannel.get(position));
    }

    @Override
    public int getItemCount() {
        return listChannel.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemVideosBinding binding;

        public ViewHolder(ItemVideosBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Channel channel) {
            GlideApp.with(itemView.getContext())
                    .load(channel.getLogo())
                    .placeholder(R.drawable.ic_twotone_ondemand_video_24)
                    .error(R.drawable.ic_twotone_ondemand_video_24)
                    .override(Target.SIZE_ORIGINAL, 150)
                    .into(binding.imageView);

            binding.tvNameChannel.setText(channel.getName());
            binding.tvUrlChannel.setText(channel.getUrl());

            binding.tvUrlChannel.setOnClickListener(v -> {
                Intent intent = new Intent(v.getContext(), PlayVideoActivity.class);
                intent.putExtra(PlayVideoActivity.EXTRA_URL, channel.getUrl());
                v.getContext().startActivity(intent);
            });
        }
    }
}
