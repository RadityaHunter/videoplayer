package com.adit.videoplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.adit.videoplayer.Adapter.ChannelAdapter;
import com.adit.videoplayer.Model.Channel;
import com.adit.videoplayer.databinding.ActivityMainBinding;
import com.adit.videoplayer.viewModel.MainViewModel;
import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private ChannelAdapter adapter;

    private String name;
    private Intent intent;
    private final ArrayList<Channel> listChannel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        adapter = new ChannelAdapter();
        setViewModel();
        setRecyclerView();

        //Sort by name
        Collections.sort(listChannel, new Comparator<Channel>() {
            @Override
            public int compare(Channel channel, Channel t1) {
                return channel.getName().compareTo(t1.getName());
            }
        });
    }

    private void setViewModel(){
        MainViewModel mainViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(MainViewModel.class);

        mainViewModel.getChannel().observe(this, channels -> {
            if(channels != null){
                adapter.setListChannel(channels);
            }
        });
    }

    private void setRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.RvListNameChannel.setLayoutManager(layoutManager);
        binding.RvListNameChannel.setHasFixedSize(true);
        binding.RvListNameChannel.setAdapter(adapter);
    }

   /* @NonNull
    private static MainViewModel obtainViewModel(AppCompatActivity activity) {
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return new ViewModelProvider(activity, factory).get(MainViewModel.class);
    }*/


}