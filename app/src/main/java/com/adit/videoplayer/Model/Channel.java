package com.adit.videoplayer.Model;

import com.google.gson.annotations.SerializedName;

public class Channel {
    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    @SerializedName("logo")
    private String logo;

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getLogo() {
        return logo;
    }
}