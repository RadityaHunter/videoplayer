package com.adit.videoplayer.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResChannel {
    @SerializedName("data")
    private ArrayList<Channel> channelArrayList;

    public ArrayList<Channel> getChannelArrayList() {
        return channelArrayList;
    }
}