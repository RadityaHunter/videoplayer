package com.adit.videoplayer.PlayVideo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.adit.videoplayer.R;
import com.adit.videoplayer.databinding.ActivityPlayVideoBinding;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;

public class PlayVideoActivity extends AppCompatActivity {
    public static final String EXTRA_URL = "extra_url";
    PlayerView playerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.adit.videoplayer.databinding.ActivityPlayVideoBinding binding = ActivityPlayVideoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        playerView = binding.exoPlayer;
        playChannel();


    }

    public void playChannel() {
        String videoURL = getIntent().getStringExtra(EXTRA_URL);

        DataSource.Factory dataSourceFactory = new DefaultHttpDataSource.Factory();

        HlsMediaSource hlsMediaSource =
                new HlsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(MediaItem.fromUri(videoURL));

        ExoPlayer player = new ExoPlayer.Builder(this).build();
        playerView.setPlayer(player);
        MediaItem mediaItem = MediaItem.fromUri(videoURL);
        player.setMediaSource(hlsMediaSource);
        player.setMediaItem(mediaItem);
        player.prepare();
        player.play();
    }
}