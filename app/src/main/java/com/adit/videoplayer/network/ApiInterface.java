package com.adit.videoplayer.network;



import com.adit.videoplayer.Model.ResChannel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("getChannelList")
    Call<ResChannel> getListChannel();

}
