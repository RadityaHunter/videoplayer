package com.adit.videoplayer.viewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.adit.videoplayer.Model.Channel;
import com.adit.videoplayer.Model.ResChannel;
import com.adit.videoplayer.network.ApiConfig;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {

    private final MutableLiveData<ArrayList<Channel>> _channel = new MutableLiveData<>();
    public LiveData<ArrayList<Channel>> getChannel() {
        return _channel;
    }

    public MainViewModel() {
        listChannel();
    }

    public void listChannel() {
        Call<ResChannel> client = ApiConfig.getApiService().getListChannel();
        client.enqueue(new Callback<ResChannel>() {
            @Override
            public void onResponse(@NonNull Call<ResChannel> call, @NonNull Response<ResChannel> response) {
                if(response.isSuccessful()){
                    if(response.body() != null){
                        _channel.postValue(response.body().getChannelArrayList());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResChannel> call, @NonNull Throwable t) {

            }
        });
    }
}
